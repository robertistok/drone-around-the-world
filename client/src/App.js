import React from "react";
import { ApolloProvider } from "react-apollo";
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks";

import { HashRouter } from "react-router-dom";

import "normalize.css/normalize.css";

import Main from "./components/Main";
import { SharedSnackbarProvider } from "./components/SharedSnackbar/SharedSnackbar.context";

import client from "./graphql";

const App = () => {
  return (
    <ApolloProvider client={client}>
      <ApolloHooksProvider client={client}>
        <SharedSnackbarProvider>
          <HashRouter>
            <Main />
          </HashRouter>
        </SharedSnackbarProvider>
      </ApolloHooksProvider>
    </ApolloProvider>
  );
};

export default App;
