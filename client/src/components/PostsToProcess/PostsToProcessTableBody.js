import React from "react";
import PropTypes from "prop-types";

import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Checkbox from "@material-ui/core/Checkbox";

const PostsToProcessTableBody = ({ isRowSelected, posts, handleRowClick }) => {
  return (
    <TableBody>
      {posts.map(row => {
        const isSelected = isRowSelected(row.id);

        return (
          <TableRow
            key={row.id}
            hover
            onClick={handleRowClick(row.id)}
            role="checkbox"
            aria-checked={isSelected}
            tabIndex={-1}
            selected={isSelected}
          >
            <TableCell padding="checkbox">
              <Checkbox checked={isSelected} />
            </TableCell>
            <TableCell component="th" scope="row">
              {row.shortcode}
            </TableCell>
            <TableCell>
              <Checkbox checked={row.isPostedByCurator} />
            </TableCell>
          </TableRow>
        );
      })}
    </TableBody>
  );
};

PostsToProcessTableBody.propTypes = {
  isRowSelected: PropTypes.func.isRequired,
  handleRowClick: PropTypes.func.isRequired,
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      isPostedByCurator: PropTypes.bool,
      id: PropTypes.string,
      shortcode: PropTypes.string
    }).isRequired
  )
};

export default PostsToProcessTableBody;
