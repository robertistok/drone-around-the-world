import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import DeleteIcon from "@material-ui/icons/Delete";
import CloudDownload from "@material-ui/icons/CloudDownload";

const PostsToProcessTableToolbar = ({ numSelected = 0, handleBatchActionClick }) => {
  return (
    <Toolbar>
      <div>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subtitle1">
            {numSelected} posts selected
          </Typography>
        ) : (
          <Typography variant="h6" id="tableTitle">
            Posts to process
          </Typography>
        )}
      </div>
      {numSelected > 0 ? (
        <ActionsWrapper>
          <Tooltip title="Delete">
            <IconButton aria-label="Delete" onClick={handleBatchActionClick("delete")}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>

          <Tooltip title="Process selected">
            <IconButton aria-label="Process" onClick={handleBatchActionClick("process")}>
              <CloudDownload />
            </IconButton>
          </Tooltip>
        </ActionsWrapper>
      ) : null}
    </Toolbar>
  );
};

PostsToProcessTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
  handleBatchActionClick: PropTypes.func.isRequired
};

const ActionsWrapper = styled.div`
  margin-left: auto;
`;

export default PostsToProcessTableToolbar;
