import React, { useState } from "react";
import styled from "styled-components";
import { useQuery, useMutation } from "react-apollo-hooks";

import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import Paper from "@material-ui/core/Paper";

import Loader from "../Loader";

import { GET_POSTS_QUERY } from "../../graphql/queries";
import { DELETE_POSTS_MUTATION, PROCESS_POSTS_MUTATION } from "../../graphql/mutations";

import PostsToProcessTableBody from "./PostsToProcessTableBody";
import PostsToProcessTableHeader from "./PostsToProcessTableHeader";
import PostsToProcessTableToolbar from "./PostsToProcessTableToolbar";

const PostsToProcess = () => {
  const {
    data: { posts = [] },
    loading
  } = useQuery(GET_POSTS_QUERY, {
    variables: { where: { mediaPath: null } }
  });
  const deletePosts = useMutation(DELETE_POSTS_MUTATION, {
    refetchQueries: [{ query: GET_POSTS_QUERY, variables: { where: { mediaPath: null } } }]
  });
  const processPosts = useMutation(PROCESS_POSTS_MUTATION, {
    refetchQueries: [{ query: GET_POSTS_QUERY, variables: { where: { mediaPath: null } } }]
  });

  const [selected, setSelected] = useState([]);

  const handleRowClick = id => event => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [...selected];

    if (selectedIndex === -1) {
      newSelected.push(id);
    } else {
      newSelected = selected.filter(rowId => rowId !== id);
    }

    setSelected(newSelected);
  };

  const handleSelectAllClick = event => {
    if (event.target.checked) {
      setSelected(posts.map(post => post.id));
    } else {
      setSelected([]);
    }
  };

  const handleBatchActionClick = action => () => {
    if (action === "delete") {
      deletePosts({ variables: { where: { id_in: selected } } });
    } else if (action === "process") {
      processPosts({ variables: { data: selected } });
    }

    setSelected([]);
  };

  const isRowSelected = id => selected.includes(id);

  const rowCount = posts.length;
  const selectedCount = selected.length;

  if (loading) {
    return <Loader />;
  }

  return (
    <Root>
      <Grid container>
        <Grid item lg={12} xs={12}>
          <Paper>
            <PostsToProcessTableToolbar
              numSelected={selectedCount}
              handleBatchActionClick={handleBatchActionClick}
            />
            <Table>
              <PostsToProcessTableHeader
                selectedCount={selectedCount}
                rowCount={rowCount}
                handleSelectAllClick={handleSelectAllClick}
              />
              <PostsToProcessTableBody
                posts={posts}
                isRowSelected={isRowSelected}
                handleRowClick={handleRowClick}
              />
            </Table>
          </Paper>
        </Grid>
      </Grid>
    </Root>
  );
};

const Root = styled.div`
  padding: 16px;
  width: 100%;
`;

export default PostsToProcess;
