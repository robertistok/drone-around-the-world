import React, { useState, useEffect } from "react";
import { Route, Redirect } from "react-router-dom";
import useReactRouter from "use-react-router";
import styled from "styled-components";

import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import Publishing from "./Publishing";
import Discovery from "./Discovery";

const ROUTES = {
  PUBLISHING: "/publishing",
  DISCOVERY: "/discovery"
};

const Home = () => {
  const { history, location } = useReactRouter();
  const [tabValue, setTabvalue] = useState(ROUTES.PUBLISHING);

  useEffect(() => setTabvalue(location.pathname || ROUTES.PUBLISHING), [location.pathname]);

  const handleTabChange = (event, value) => {
    history.push(value);
    setTabvalue(value);
  };

  return (
    <div>
      <AppBar position="static">
        <Tabs value={tabValue} variant="fullWidth" onChange={handleTabChange}>
          <Tab value={ROUTES.PUBLISHING} label="Publishing" />
          <Tab value={ROUTES.DISCOVERY} label="Discovery" />
        </Tabs>
      </AppBar>

      <Body>
        <Route path={ROUTES.PUBLISHING} component={Publishing} />
        <Route path={ROUTES.DISCOVERY} component={Discovery} />
        <Route exact path="/" render={() => <Redirect to={ROUTES.PUBLISHING} />} />
      </Body>
    </div>
  );
};

const Body = styled.div`
  padding: 20px 30px;
`;

export default Home;
