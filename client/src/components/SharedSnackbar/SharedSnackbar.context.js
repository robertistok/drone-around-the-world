import React, { useState } from "react";

import SharedSnackbar from "./SharedSnackbar";

export const SharedSnackbarContext = React.createContext();

export const SharedSnackbarProvider = ({ children }) => {
  const [state, setState] = useState({
    open: false,
    message: "",
    color: undefined
  });

  const closeSnackbar = () =>
    setState({
      open: false,
      color: undefined,
      message: undefined
    });

  const updateSnackbar = ({ open, color, message }) =>
    setState({
      open: open || state.open,
      color: color || state.color,
      message: message || state.message
    });

  return (
    <SharedSnackbarContext.Provider
      value={{
        closeSnackbar: closeSnackbar,
        updateSnackbar: updateSnackbar,
        ...state
      }}
    >
      <SharedSnackbar />
      {children}
    </SharedSnackbarContext.Provider>
  );
};

export const SharedSnackbarConsumer = SharedSnackbarContext.Consumer;
