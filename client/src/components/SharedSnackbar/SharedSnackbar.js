import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import { withStyles } from "@material-ui/core/styles";

// import { colorPalette } from "../../../common/styles";

import { SharedSnackbarConsumer } from "./SharedSnackbar.context";

const SharedSnackbar = ({ classes }) => {
  return (
    <SharedSnackbarConsumer>
      {({ color, open, message, closeSnackbar, autoHideDuration = 1000 }) => (
        <Snackbar
          color={color}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={open}
          autoHideDuration={autoHideDuration}
          onClose={closeSnackbar}
          ContentProps={{
            "aria-describedby": "message-id",
            classes: { root: color && classes[color] }
          }}
          message={<span id="message-id">{message}</span>}
        />
      )}
    </SharedSnackbarConsumer>
  );
};

const styles = {
  // ...Object.entries(colorPalette).reduce(
  //   (acc, [name, color]) => ({
  //     ...acc,
  //     [name]: { "background-color": color }
  //   }),
  //   {}
  // )
};

export default withStyles(styles)(SharedSnackbar);
