import React from "react";
import PropTypes from "prop-types";

import TableBody from "@material-ui/core/TableBody";

import Post from "./Post";

const PostsToPublishTableBody = ({ isRowSelected, posts, handleRowClick }) => {
  return (
    <TableBody>
      {posts.map(post => (
        <Post
          key={post.id}
          isSelected={isRowSelected(post.id)}
          handleRowClick={handleRowClick}
          mediaPath={post.mediaPath}
          captionForFacebook={post.captionForFacebook}
          dateTimeOfPosting={post.dateTimeOfPosting}
          shortcode={post.shortcode}
          isPublished={post.isPublished}
          id={post.id}
        />
      ))}
    </TableBody>
  );
};

PostsToPublishTableBody.propTypes = {
  isRowSelected: PropTypes.func.isRequired,
  handleRowClick: PropTypes.func.isRequired,
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      isPublished: PropTypes.bool,
      id: PropTypes.string,
      shortcode: PropTypes.string,
      mediaPath: PropTypes.string,
      dateTimeOfPosting: PropTypes.string,
      captionForFacebook: PropTypes.string
    }).isRequired
  )
};

export default PostsToPublishTableBody;
