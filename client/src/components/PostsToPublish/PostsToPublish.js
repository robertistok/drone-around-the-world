import React, { useState, useContext } from "react";
import styled from "styled-components";
import { useQuery, useMutation } from "react-apollo-hooks";

import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TablePagination from "@material-ui/core/TablePagination";

import Paper from "@material-ui/core/Paper";

import Loader from "../Loader";

import { GET_POSTS_QUERY } from "../../graphql/queries";
import { DELETE_POSTS_MUTATION } from "../../graphql/mutations";
import { SharedSnackbarContext } from "../SharedSnackbar/SharedSnackbar.context";

import PostsToPublishTableBody from "../PostsToPublish/PostsToPublishTableBody";
import PostsToPublishTableHeader from "../PostsToPublish/PostsToPublishTableHeader";
import PostsToPublishTableToolbar from "../PostsToPublish/PostsToPublishTableToolbar";

const rowsPerPage = 20;

const PostsToPublish = () => {
  const [page, setPage] = useState(0);
  const [lastFetchedPage, setLastFetchedPage] = useState(0);

  const {
    data: { posts = [], aggregatePosts },
    loading,
    fetchMore
  } = useQuery(GET_POSTS_QUERY, {
    variables: {
      first: rowsPerPage,
      where: { mediaPath_not: "", isPublished: false }
    }
  });
  const deletePosts = useMutation(DELETE_POSTS_MUTATION);

  const [selected, setSelected] = useState([]);
  const { updateSnackbar } = useContext(SharedSnackbarContext);

  const handleRowClick = id => event => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [...selected];

    if (selectedIndex === -1) {
      newSelected.push(id);
    } else {
      newSelected = selected.filter(rowId => rowId !== id);
    }

    setSelected(newSelected);
  };

  const handleSelectAllClick = event => {
    if (event.target.checked) {
      setSelected(posts.map(post => post.id));
    } else {
      setSelected([]);
    }
  };

  const handleBatchActionClick = action => () => {
    if (action === "delete") {
      deletePosts({
        variables: { where: { id_in: selected } },
        update: async () => {
          await fetchMore({
            variables: {
              first: rowsPerPage,
              skip: rowsPerPage * page
            },
            updateQuery: (prev, { fetchMoreResult }) => {
              const existingPosts = prev.posts.filter(post => !selected.includes(post.id));
              const newPosts = existingPosts
                .concat(fetchMoreResult.posts)
                .reduce(
                  (acc, post) => (acc.find(i => i.id === post.id) ? acc : [...acc, post]),
                  []
                );

              return {
                ...prev,
                ...fetchMoreResult,
                posts: newPosts
              };
            }
          });

          updateSnackbar({ open: true, message: `${selected} posts deleted successfully` });
        }
      });
    }

    setSelected([]);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);

    if (lastFetchedPage < newPage) {
      setLastFetchedPage(newPage);

      fetchMore({
        variables: {
          first: rowsPerPage,
          skip: rowsPerPage * newPage
        },
        updateQuery: (prev, { fetchMoreResult }) =>
          fetchMoreResult ? { ...prev, posts: [...prev.posts, ...fetchMoreResult.posts] } : prev
      });
    }
  };

  const isRowSelected = id => selected.includes(id);

  const rowCount = posts.length;
  const selectedCount = selected.length;
  const postsToDisplay = posts.filter(({ isPublished }) => !isPublished);
  const rows = postsToDisplay.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

  if (loading) {
    return <Loader />;
  }

  return (
    <Root>
      <Grid container>
        <Grid item lg={12} xs={12}>
          <Paper>
            <PostsToPublishTableToolbar
              numSelected={selectedCount}
              handleBatchActionClick={handleBatchActionClick}
            />
            <Table>
              <PostsToPublishTableHeader
                selectedCount={selectedCount}
                rowCount={rowCount}
                handleSelectAllClick={handleSelectAllClick}
              />
              <PostsToPublishTableBody
                posts={rows}
                isRowSelected={isRowSelected}
                handleRowClick={handleRowClick}
              />
            </Table>
            <TablePagination
              component="div"
              count={aggregatePosts.totalCount}
              rowsPerPage={rowsPerPage}
              rowsPerPageOptions={[rowsPerPage]}
              page={page}
              backIconButtonProps={{
                "aria-label": "Previous Page"
              }}
              nextIconButtonProps={{
                "aria-label": "Next Page"
              }}
              onChangePage={handleChangePage}
            />
          </Paper>
        </Grid>
      </Grid>
    </Root>
  );
};

const Root = styled.div`
  padding: 16px;
  width: 100%;
`;

export default PostsToPublish;
