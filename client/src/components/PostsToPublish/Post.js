import React, { useState, useContext } from "react";
import { useMutation } from "react-apollo-hooks";
import gql from "graphql-tag";

import TextField from "@material-ui/core/TextField";
import Fab from "@material-ui/core/Fab";
import Checkbox from "@material-ui/core/Checkbox";
import DesktopMac from "@material-ui/icons/DesktopMac";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Tooltip from "@material-ui/core/Tooltip";

import { SharedSnackbarContext } from "../SharedSnackbar/SharedSnackbar.context";

const remote = window.require("electron").remote;
const { shell, clipboard } = remote;

const UPDATE_POST_MUTATION = gql`
  mutation UPDATE_POST_MUTATION(
    $id: ID!
    $captionForFacebook: String
    $isPublished: Boolean
    $dateTimeOfPosting: String
  ) {
    updatePost(
      where: { id: $id }
      data: {
        captionForFacebook: $captionForFacebook
        isPublished: $isPublished
        dateTimeOfPosting: $dateTimeOfPosting
      }
    ) {
      id
      captionForFacebook
      isPublished
      dateTimeOfPosting
    }
  }
`;

const Post = ({
  mediaPath,
  captionForFacebook,
  id,
  isPublished,
  dateTimeOfPosting,
  isSelected,
  handleRowClick
}) => {
  const updatePost = useMutation(UPDATE_POST_MUTATION);
  const [inputState, setInputState] = useState({
    captionForFacebook,
    isPublished,
    dateTimeOfPosting
  });
  const { updateSnackbar } = useContext(SharedSnackbarContext);

  const handleInputChange = event => {
    event.persist();

    const { target } = event;
    const value = target.type === "checkbox" ? target.checked : target.value;

    const newInputState = { [target.name]: value };

    updatePost({
      variables: { id, ...newInputState }
    });

    setInputState({ ...inputState, ...newInputState });
  };

  const copyToClipboard = event => {
    event.persist();
    clipboard.writeText(event.target.value);
    updateSnackbar({ open: true, message: `${event.target.name} copied to clipboard` });
  };

  return (
    <TableRow hover role="checkbox" aria-checked={isSelected} tabIndex={-1} selected={isSelected}>
      <TableCell padding="checkbox" onClick={handleRowClick(id)}>
        <Checkbox checked={isSelected} />
      </TableCell>
      <TableCell component="th" scope="row">
        <Fab aria-label="Show in Finder">
          <DesktopMac onClick={() => shell.showItemInFolder(mediaPath)} />
        </Fab>
      </TableCell>
      <TableCell>
        <Tooltip onClick={copyToClipboard} title="Click me to copy" placement="top">
          <TextField
            id="caption-for-facebook"
            name="captionForFacebook"
            placeholder="No caption..."
            multiline
            margin="normal"
            fullWidth
            value={inputState.captionForFacebook}
            onChange={handleInputChange}
          />
        </Tooltip>
      </TableCell>

      <TableCell>
        <Tooltip onClick={copyToClipboard} title="Click me to copy" placement="top">
          <TextField
            id="dateTimeOfPosting"
            name="dateTimeOfPosting"
            placeholder="No date&time is available"
            margin="normal"
            fullWidth
            value={inputState.dateTimeOfPosting}
            onChange={handleInputChange}
          />
        </Tooltip>
      </TableCell>

      <TableCell>
        <Checkbox
          name="isPublished"
          checked={inputState.isPublished}
          onChange={handleInputChange}
        />
      </TableCell>
    </TableRow>
  );
};

export default Post;
