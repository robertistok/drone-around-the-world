import React from "react";
import PropTypes from "prop-types";

import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Checkbox from "@material-ui/core/Checkbox";

const PostsToPublishTableHeader = ({ selectedCount = 0, rowCount = 0, handleSelectAllClick }) => {
  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={selectedCount > 0 && selectedCount < rowCount}
            checked={selectedCount === rowCount && rowCount !== 0}
            onChange={handleSelectAllClick}
          />
        </TableCell>
        <TableCell>Show in finder</TableCell>
        <TableCell>Caption for FB</TableCell>
        <TableCell>Datetime</TableCell>
        <TableCell>Published</TableCell>
      </TableRow>
    </TableHead>
  );
};

PostsToPublishTableHeader.propTypes = {
  selectedCount: PropTypes.number.isRequired,
  rowCount: PropTypes.number.isRequired,
  handleSelectAllClick: PropTypes.func.isRequired
};

export default PostsToPublishTableHeader;
