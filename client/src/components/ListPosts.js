import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import styled from "styled-components";

import Post from "./Post";
import Loader from "./Loader";

import { GET_POSTS_QUERY } from "../graphql/queries";

const ListPosts = () => {
  return (
    <Query query={GET_POSTS_QUERY} variables={{ where: { mediaPath_not: "", isPublished: false } }}>
      {({ data: { posts }, loading }) => {
        if (loading) {
          return <Loader />;
        }

        return (
          <Root>
            {posts.map(post => (
              <Post key={post.id} {...post} />
            ))}
          </Root>
        );
      }}
    </Query>
  );
};

const Root = styled.section`
  margin: 10px 30px;
`;

export default ListPosts;
