import gql from "graphql-tag";
import React, { useState } from "react";
import { useMutation } from "react-apollo-hooks";
import { animated, useTransition } from "react-spring";
import styled from "styled-components";

import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

import NewPost from "./NewPost";

import { GET_POSTS_QUERY } from "../graphql/queries";

const ADD_NEW_POSTS_MUTATION = gql`
  mutation ADD_NEW_POSTS_MUTATION($data: [PostCreateInput!]) {
    createPosts(data: $data) {
      id
      captionForFacebook
      isPublished
    }
  }
`;

const AddNewPosts = () => {
  const initialPostState = { shortcode: "", isPostedByCurator: true, updated: false };
  const [postsToAdd, setPostsToAdd] = useState([initialPostState]);

  const addNewPosts = useMutation(ADD_NEW_POSTS_MUTATION, {
    refetchQueries: [{ query: GET_POSTS_QUERY, variables: { where: { mediaPath: null } } }]
  });

  const transitions = useTransition(postsToAdd, (post, index) => index, {
    from: { transform: "translate3d(-100px, 0px, 0)" },
    enter: { transform: "translate3d(0px, 0px, 0)" },
    leave: { transform: "translate3d(0px, 0px, 0)" }
  });

  const handleInputChange = indexToUpdate => event => {
    event.persist();

    const { target } = event;

    const value = target.type === "checkbox" ? target.checked : target.value;

    const newPostsToAdd = postsToAdd.map((post, index) =>
      index === indexToUpdate ? { ...post, [target.name]: value, updated: true } : post
    );

    // if the last post is edited, we want to add a new field for the next one
    if (
      indexToUpdate === newPostsToAdd.length - 1 &&
      postsToAdd[indexToUpdate].updated === false &&
      newPostsToAdd[indexToUpdate].updated === true
    ) {
      newPostsToAdd.push(initialPostState);
    }

    setPostsToAdd(newPostsToAdd);
  };

  const handleSave = () => {
    const newPosts = postsToAdd
      .filter(post => Boolean(post.shortcode))
      .map(({ updated, ...post }) => post);

    addNewPosts({ variables: { data: newPosts } });
    setPostsToAdd([initialPostState]);
  };

  return (
    <Root>
      <Grid item={true} xs={12}>
        <Typography component="h2" variant="headline" gutterBottom={true}>
          Add new posts by shortcode
        </Typography>
      </Grid>

      {transitions.map(
        ({ item: post, props, ...rest }, index) =>
          console.log(rest) || (
            <animated.div style={props} key={index}>
              <NewPost handleInputChange={handleInputChange(index)} {...post} />
            </animated.div>
          )
      )}

      <Grid item={true} xs={12}>
        <Button variant="contained" color="primary" onClick={handleSave}>
          Save
        </Button>
      </Grid>
    </Root>
  );
};

const Root = styled.div`
  padding: 16px;
  width: 100%;
`;

export default AddNewPosts;
