import React, { FunctionComponent } from "react";

import PostsToPublish from "./PostsToPublish/PostsToPublish";

const Publishing: FunctionComponent = () => {
  return (
    <div>
      <PostsToPublish />
    </div>
  );
};

export default Publishing;
