import React, { ChangeEventHandler, FunctionComponent } from "react";
import styled from "styled-components";

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";

interface AddNewPostProps {
  handleInputChange: ChangeEventHandler;
  shortcode: string;
  isPostedByCurator: boolean;
}

const AddNewPost: FunctionComponent<AddNewPostProps> = ({
  handleInputChange,
  shortcode,
  isPostedByCurator
}) => {
  return (
    <Root>
      <Grid container={true} spacing={32} alignContent="center" alignItems="flex-end">
        <Grid item={true} lg={3} xs={6}>
          <TextField
            id="add-new-shortcode"
            name="shortcode"
            label="Add new post"
            placeholder="insert the shortcode"
            margin="normal"
            fullWidth={true}
            value={shortcode}
            onChange={handleInputChange}
          />
        </Grid>

        <Grid item={true} xs={6}>
          <FormControlLabel
            control={
              <Checkbox
                name="isPostedByCurator"
                checked={isPostedByCurator}
                onChange={handleInputChange}
              />
            }
            label="Posted by curator"
            name="isPostedByCurator"
          />
        </Grid>
      </Grid>
    </Root>
  );
};

const Root = styled.div`
  padding: 16px;
  width: 100%;
`;

export default AddNewPost;
