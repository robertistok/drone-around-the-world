import React from "react";

import Grid from "@material-ui/core/Grid";

import AddNewPosts from "./AddNewPosts";
import PostsToProcess from "./PostsToProcess/PostsToProcess";

const Discovery = () => {
  return (
    <Grid container spacing={32} alignContent="center" alignItems="flex-end">
      <AddNewPosts />
      <PostsToProcess />
    </Grid>
  );
};

export default Discovery;
