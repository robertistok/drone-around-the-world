import gql from "graphql-tag";

export const DELETE_POSTS_MUTATION = gql`
  mutation DELETE_POSTS_MUTATION($where: PostWhereInput!) {
    deleteManyPosts(where: $where) {
      count
    }
  }
`;

export const PROCESS_POSTS_MUTATION = gql`
  mutation PROCESS_POSTS_MUTATION($data: [ID!]!) {
    processPosts(data: $data) {
      id
      isProcessed
    }
  }
`;
