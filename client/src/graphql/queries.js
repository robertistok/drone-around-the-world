import gql from "graphql-tag";

export const GET_POSTS_QUERY = gql`
  query GET_POSTS_QUERY($where: PostWhereInput, $first: Int, $skip: Int) {
    posts(where: $where, first: $first, skip: $skip) {
      id
      shortcode
      mediaPath
      captionForFacebook
      isPublished
      isPostedByCurator
      dateTimeOfPosting
    }

    aggregatePosts(where: $where) {
      totalCount
    }
  }
`;
