const { forwardTo } = require("prisma-binding");

const Query = {
  posts: forwardTo("db"),
  postsConnection: forwardTo("db"),
  aggregatePosts: async (parent, args, ctx, info) => {
    const postsConnection = await ctx.db.query.postsConnection(
      { where: args.where },
      `{ aggregate { count } }`,
      info
    );

    return { totalCount: postsConnection.aggregate.count };
  }
};

module.exports = Query;
