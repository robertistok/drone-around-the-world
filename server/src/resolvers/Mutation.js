const { forwardTo } = require("prisma-binding");
const fs = require("fs");

const { processPost } = require("../lib/post");

const Mutation = {
  deletePost: forwardTo("db"),
  updateManyPosts: forwardTo("db"),
  updatePost: forwardTo("db"),

  createPosts: async (parent, args, ctx, info) => {
    const newPosts = await Promise.all(
      args.data.map(post => ctx.db.mutation.createPost({ data: post }, info))
    );

    return newPosts;
  },

  processPosts: async (parent, args, ctx, info) => {
    const [posts, lastScheduledPost] = await Promise.all([
      ctx.db.query.posts(
        { where: { id_in: args.data } },
        `{ id, shortcode, isPostedByCurator }`,
        info
      ),
      ctx.db.query.posts(
        {
          orderBy: "dateTimeOfPosting_DESC",
          first: 1
        },
        `{ id, dateTimeOfPosting }`,
        info
      )
    ]);

    const processedPosts = await Promise.all(
      posts.map(async (post, index) => {
        const processedPost = await processPost({ ...post, index, lastScheduledPost });

        await ctx.db.mutation.updatePost(
          {
            where: { id: post.id },
            data: processedPost
          },
          info
        );

        return { ...post, ...processedPost };
      })
    );

    return processedPosts;
  },

  deleteManyPosts: async (parent, args, ctx, info) => {
    const postsToDelete = await ctx.db.query.posts(args, `{ id, mediaPath }`, info);

    // delete the files from the filesystem if it exists
    postsToDelete.forEach(({ mediaPath }) => {
      if (fs.existsSync(mediaPath)) {
        fs.unlink(mediaPath, err => {
          if (err) {
            console.log(err);
            return;
          }
          console.log(`File ${mediaPath} succesfully deleted`);
        });
      } else {
        console.log(`This file ${mediaPath} doesn't exist, cannot delete`);
      }
    });

    return forwardTo("db")(parent, args, ctx, info);
  }
};

module.exports = Mutation;
