const { getRandomTimeBetween, possibleMinuteCondition } = require("../lib/randomize");

describe("randomize function", () => {
  it("gets a random time between the specified period, start <= end", () => {
    const start = { hour: 8, minute: 50 };
    const end = { hour: 10, minute: 22 };
    const randomTimes = [];

    for (let i = 0; i < 1000; i += 1) {
      randomTimes.push(getRandomTimeBetween({ start, end }));
    }

    randomTimes.forEach(randomTime => {
      expect(randomTime.hour >= start.hour).toBe(true);
      expect(
        possibleMinuteCondition({
          randomHour: randomTime.hour,
          minuteToCompare: randomTime.minute,
          startHour: start.hour,
          endHour: end.hour,
          startMinute: start.minute,
          endMinute: end.minute
        })
      ).toBe(true);
    });
  });

  it("gets a random time between the specified period, startH > endH", () => {
    const start = { hour: 8, minute: 50 };
    const end = { hour: 2, minute: 30 };
    const randomTimes = [];

    for (let i = 0; i < 1000; i += 1) {
      randomTimes.push(getRandomTimeBetween({ start, end }));
    }

    randomTimes.forEach(randomTime => {
      expect(randomTime.hour >= start.hour || randomTime.hour <= end.hour).toBe(true);
      expect(
        possibleMinuteCondition({
          randomHour: randomTime.hour,
          minuteToCompare: randomTime.minute,
          startHour: start.hour,
          endHour: end.hour,
          startMinute: start.minute,
          endMinute: end.minute
        })
      ).toBe(true);
    });
  });
});
