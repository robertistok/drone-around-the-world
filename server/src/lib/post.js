const moment = require("moment");

const { getRandomTimeBetween } = require("../lib/randomize");
const { getPostData } = require("./scrape");
const downloadMedia = require("./downloadMedia");

const POST_BASE_URL = "https://www.instagram.com";

const expectedMomentFormat = "MMM_DD";
const postsAttributes = [
  {
    id: "a",
    start: {
      hour: 8,
      minute: 30
    },
    end: {
      hour: 9,
      minute: 30
    }
  },
  // {
  //   id: "b",
  //   start: {
  //     hour: 11,
  //     minute: 30
  //   },
  //   end: {
  //     hour: 12,
  //     minute: 30
  //   }
  // },
  {
    id: "b",
    start: {
      hour: 16,
      minute: 30
    },
    end: {
      hour: 17,
      minute: 30
    }
  }
  // {
  //   id: "d",
  //   start: {
  //     hour: 19,
  //     minute: 30
  //   },
  //   end: {
  //     hour: 20,
  //     minute: 30
  //   }
  // }
];

const getCreatorInstagramProfile = ({ caption, isPostedByCurator, username }) => {
  let originalCreator = username;

  if (isPostedByCurator) {
    let usernameFromCaption;
    const indexOfAtSign = caption.indexOf("@");

    if (indexOfAtSign === -1) {
      usernameFromCaption = undefined;
    } else {
      usernameFromCaption = caption
        .substr(indexOfAtSign)
        .split(" ")[0]
        .replace("@", "");
    }

    originalCreator = usernameFromCaption || username;
  }

  const creatorProfile = `${POST_BASE_URL}/${originalCreator}`;

  return creatorProfile;
};

const getCaptionForFacebook = ({ location, caption, isPostedByCurator, username }) => {
  const creatorProfile = getCreatorInstagramProfile({ caption, isPostedByCurator, username });
  const captionForFacebook = `${location}\n\nCredits to ${creatorProfile}`;

  return captionForFacebook;
};

const processPost = async ({ isPostedByCurator, shortcode, index, lastScheduledPost }) => {
  const { start, end, id } = postsAttributes[index % 2];
  const dateOfPosting = moment(lastScheduledPost.dateTimeOfPosting).add(
    Math.floor(index / 2) + 1,
    "days"
  );
  const timeOfPosting = getRandomTimeBetween({ start, end, padStart: true });
  const datePartOfTheFilename = dateOfPosting.format(expectedMomentFormat);
  const dateTimeOfPosting = `${dateOfPosting.format("DD/MM/YYYY")}-${timeOfPosting.hour}:${
    timeOfPosting.minute
  }`;
  const filename = `${datePartOfTheFilename}_${id}_${new Date().getTime()}.jpg`.toLowerCase();

  try {
    const post = await getPostData({ shortcode });
    const downloadedFilePath = await downloadMedia({ url: post.imageUrl, filename });
    const captionForFacebook = getCaptionForFacebook({
      ...post,
      isPostedByCurator
    });

    console.log(`${shortcode} just got processed`);

    return {
      mediaPath: downloadedFilePath,
      captionForFacebook,
      dateTimeOfPosting,
      isProcessed: true
    };
  } catch (err) {
    console.log(err);
  }
};

module.exports = { getCaptionForFacebook, processPost };
