const HOURS = [
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
  21,
  22,
  23,
  24
];

const MINUTES = [
  0,
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
  21,
  22,
  23,
  24,
  25,
  26,
  27,
  28,
  29,
  30,
  31,
  32,
  33,
  34,
  35,
  36,
  37,
  38,
  39,
  40,
  41,
  42,
  43,
  44,
  45,
  46,
  47,
  48,
  49,
  50,
  51,
  52,
  53,
  54,
  55,
  56,
  57,
  58,
  59
];

const getRandomInt = (min, max) => {
  const minCeiled = Math.ceil(min);
  const maxFloored = Math.floor(max);

  return Math.floor(Math.random() * (maxFloored - minCeiled)) + minCeiled;
};

const possibleMinuteCondition = ({
  randomHour,
  startHour,
  endHour,
  startMinute,
  endMinute,
  minuteToCompare
}) => {
  if (randomHour === startHour) {
    if (startHour === endHour) {
      return minuteToCompare >= startMinute && minuteToCompare <= endMinute;
    }
    return minuteToCompare >= startMinute;
  }
  if (randomHour === endHour) {
    return minuteToCompare <= endMinute;
  }
  return true;
};

// handle invalid values
const getRandomTimeBetween = ({ start, end, padStart = false }) => {
  const startHour = start.hour;
  const startMinute = start.minute;
  const endHour = end.hour;
  const endMinute = end.minute;

  const possibleHourCondition = hour =>
    startHour <= endHour
      ? hour >= startHour && hour <= endHour
      : hour >= startHour || hour <= endHour;
  const possibleHours = HOURS.filter(possibleHourCondition);
  const randomHourIndex = getRandomInt(0, possibleHours.length);
  const randomHour = possibleHours[randomHourIndex];

  const possibleMinutes = MINUTES.filter(minute =>
    possibleMinuteCondition({
      minuteToCompare: minute,
      randomHour,
      startHour,
      startMinute,
      endHour,
      endMinute
    })
  );
  const randomMinuteIndex = getRandomInt(0, possibleMinutes.length);
  const randomMinute = possibleMinutes[randomMinuteIndex];

  if (padStart) {
    return {
      hour: `${randomHour}`.padStart(2, "0"),
      minute: `${randomMinute}`.padStart(2, "0")
    };
  }

  return { hour: randomHour, minute: randomMinute };
};

module.exports = { getRandomTimeBetween, possibleMinuteCondition };
